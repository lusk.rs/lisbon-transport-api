﻿using MongoDB.Driver;
using N2L.PublicTransport.Domain.Repository;
using N2L.PublicTransport.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace N2L.PublicTransport.Infra.Data.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly IMongoContext _context;
        protected readonly IMongoCollection<T> DbSet;


        protected BaseRepository(IMongoContext context)
        {
            _context = context;
            DbSet = _context.GetCollection<T>(typeof(T).Name);
        }

        public Task Add(T item)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> Get(Expression<Func<T, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetById(string id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> PatchUpdate(string id, string body)
        {
            throw new NotImplementedException();
        }

        public Task<bool> PutUpdate(string id, T document)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Remove(string id)
        {
            throw new NotImplementedException();
        }
    }
}
