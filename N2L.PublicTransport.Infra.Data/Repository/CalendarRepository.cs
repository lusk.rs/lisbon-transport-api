﻿using N2L.PublicTransport.Domain.Entities;
using N2L.PublicTransport.Domain.Repository;
using N2L.PublicTransport.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace N2L.PublicTransport.Infra.Data.Repository
{
    public class CalendarRepository:BaseRepository<Calendar>, ICalendarRepository
    {
        public CalendarRepository(IMongoContext context):base(context)
        {

        }
    }
}
