﻿using System;
using System.Collections.Generic;
using System.Text;

namespace N2L.PublicTransport.Infrastructure.Providers
{
    public class MongoDBProvider
    {
        public string ConnectionString;
        public string Database;
    }
}
