﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using N2L.PublicTransport.Infrastructure.Providers;
using System;
using System.Collections.Generic;
using System.Text;

namespace N2L.PublicTransport.Infrastructure.Contexts
{
  


    public class MongoDBContext: IMongoContext
    {
        private readonly IMongoDatabase _database;

        public MongoDBContext(IOptions<MongoDBProvider>settings, string mongoCollection)
        {
            var client = new MongoClient(settings.Value.ConnectionString);

            _database = client?.GetDatabase(settings.Value.Database) ?? null;
        }


        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _database.GetCollection<T>(name);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }


    }
}
