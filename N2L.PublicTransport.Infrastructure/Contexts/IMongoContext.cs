﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace N2L.PublicTransport.Infrastructure.Contexts
{
    public interface IMongoContext
    {
        IMongoCollection<T> GetCollection<T>(string name);
    }
}
