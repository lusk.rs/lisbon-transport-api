﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Flurl;
using Flurl.Http;
using System.Net.Http;
using System.Text;
using RestSharp;

namespace N2L.PublicTransport.API.Comboios.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StationsController : Controller
    {



        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetStation(int id)
        {
            //var result = await RequestToken();
            var result = await GetToken();

            var request = await GetRequest("https://api.cp.pt/cp-api/siv/stations/", result);
            request = await GetRequest("https://api.cp.pt/cp-api/siv/stations/94-31047/timetable/2019-04-22", result);


            return Ok(request);
        }

        private async Task<Token> GetToken()
        {
            var result = await "https://api.cp.pt/cp-api/oauth/token"
                .WithHeaders(new { Accept = "application/json", Authorization = "Basic Y3AtbW9iaWxlOnBhc3M=" })
                .PostUrlEncodedAsync(new { grant_type = "client_credentials" })
                ;

            return await result.Content.ReadAsAsync<Token>();
        }


        private async Task<string> GetRequest(string url, Token token)
        {
            var result = await url.WithHeaders(new { Authorization = $"Bearer {token.access_token}", Accept = "application/json" }).GetAsync();
            return await result.Content.ReadAsStringAsync();
        }

        private async Task<object> PostRequest(string url, object content, Token token)
        {
            var result = await url.WithHeaders(new { Authorization = $"Bearer  {token.access_token}", Accept = "application/json", }).PostJsonAsync(content);

            return result;
        }

    }





    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string scope { get; set; }
    }


}