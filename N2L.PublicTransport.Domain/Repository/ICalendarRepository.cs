﻿using N2L.PublicTransport.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace N2L.PublicTransport.Domain.Repository
{
    public interface ICalendarRepository : IBaseRepository<Calendar>
    {
    }
}
