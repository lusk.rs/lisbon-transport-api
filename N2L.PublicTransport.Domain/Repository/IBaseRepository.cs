﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace N2L.PublicTransport.Domain.Repository
{
    public interface IBaseRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(string id);

        // query after multiple parameters
        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> expression);

        // add new note document
        Task Add(T item);

        // remove a single document / note
        Task<bool> Remove(string id);

        // update just a single document / note
        Task<bool> PatchUpdate(string id, string body);

        // demo interface - full document update
        Task<bool> PutUpdate(string id, T document);

       
    }
}
